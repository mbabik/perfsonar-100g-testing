[root@psl01-gva ~]# pscheduler task --slip PT60M   --tool iperf2 throughput --source psl01-gva-100g.cern.ch --dest perfsonar-bandwidth.grid.surfsara.nl  --ip-version=4 -t 40 -i 1  -w 536M --parallel 8
Submitting task...
Task URL:
https://psl01-gva-100g.cern.ch/pscheduler/tasks/c174b817-0783-4e48-a582-5da9717ed7bf
Running with tool 'iperf2'
Fetching first run...

Next scheduled run:
https://psl01-gva-100g.cern.ch/pscheduler/tasks/c174b817-0783-4e48-a582-5da9717ed7bf/runs/e2ac69a2-4ae4-44ab-8ca0-fdf96b88bf68
Starts 2019-05-27T17:02:23+02:00 (~60 seconds)
Ends   2019-05-27T17:03:08+02:00 (~44 seconds)
Waiting for result...

Run did not complete: Failed


Diagnostics from psl01-gva-100g.cern.ch:
  /usr/bin/iperf -p 5001 -c 145.100.32.32 -t 40 -m -i 1 -P 8 -w 536000000

  ------------------------------------------------------------
  Client connecting to 145.100.32.32, TCP port 5001
  TCP window size: 1022 MByte (WARNING: requested  511 MByte)
  ------------------------------------------------------------
  [  8] local 128.142.208.137 port 46576 connected with 145.100.32.32 port 5001
  [  7] local 128.142.208.137 port 46562 connected with 145.100.32.32 port 5001
  [  6] local 128.142.208.137 port 46563 connected with 145.100.32.32 port 5001
  [  3] local 128.142.208.137 port 46575 connected with 145.100.32.32 port 5001
  [ 10] local 128.142.208.137 port 46582 connected with 145.100.32.32 port 5001
  [  9] local 128.142.208.137 port 46565 connected with 145.100.32.32 port 5001
  [  4] local 128.142.208.137 port 46574 connected with 145.100.32.32 port 5001
  [  5] local 128.142.208.137 port 46564 connected with 145.100.32.32 port 5001
  [ ID] Interval       Transfer     Bandwidth
  [  7]  0.0- 1.0 sec  1.43 GBytes  12.3 Gbits/sec
  [  6]  0.0- 1.0 sec  1.97 GBytes  17.0 Gbits/sec
  [  4]  0.0- 1.0 sec  1.26 GBytes  10.8 Gbits/sec
  [  3]  0.0- 1.0 sec  1.27 GBytes  10.9 Gbits/sec
  [  5]  0.0- 1.0 sec  1.29 GBytes  11.1 Gbits/sec
  [ 10]  0.0- 1.0 sec  1.93 GBytes  16.6 Gbits/sec
  [  9]  0.0- 1.0 sec  1.31 GBytes  11.3 Gbits/sec
  [  8]  0.0- 1.0 sec  1.58 GBytes  13.6 Gbits/sec
  [SUM]  0.0- 1.0 sec  12.1 GBytes   104 Gbits/sec
  [  6]  1.0- 2.0 sec  1.74 GBytes  14.9 Gbits/sec
  [ 10]  1.0- 2.0 sec  1.62 GBytes  13.9 Gbits/sec
  [  9]  1.0- 2.0 sec   860 MBytes  7.21 Gbits/sec
  [  8]  1.0- 2.0 sec   514 MBytes  4.31 Gbits/sec
  [  7]  1.0- 2.0 sec   701 MBytes  5.88 Gbits/sec
  [  4]  1.0- 2.0 sec   892 MBytes  7.48 Gbits/sec
  [  5]  1.0- 2.0 sec   896 MBytes  7.52 Gbits/sec
  [  3]  1.0- 2.0 sec   907 MBytes  7.61 Gbits/sec
  [SUM]  1.0- 2.0 sec  8.01 GBytes  68.8 Gbits/sec
  [  8]  2.0- 3.0 sec   964 MBytes  8.08 Gbits/sec
  [  7]  2.0- 3.0 sec   933 MBytes  7.83 Gbits/sec
  [  6]  2.0- 3.0 sec  1.49 GBytes  12.8 Gbits/sec
  [  4]  2.0- 3.0 sec   574 MBytes  4.82 Gbits/sec
  [  3]  2.0- 3.0 sec   459 MBytes  3.85 Gbits/sec
  [ 10]  2.0- 3.0 sec  1.37 GBytes  11.8 Gbits/sec
  [  5]  2.0- 3.0 sec   541 MBytes  4.54 Gbits/sec
  [  9]  2.0- 3.0 sec   677 MBytes  5.68 Gbits/sec
  [SUM]  2.0- 3.0 sec  6.91 GBytes  59.4 Gbits/sec
  [  8]  3.0- 4.0 sec   560 MBytes  4.70 Gbits/sec
  [  6]  3.0- 4.0 sec  1.96 GBytes  16.8 Gbits/sec
  [ 10]  3.0- 4.0 sec  1.48 GBytes  12.7 Gbits/sec
  [  9]  3.0- 4.0 sec   638 MBytes  5.35 Gbits/sec
  [  7]  3.0- 4.0 sec   538 MBytes  4.51 Gbits/sec
  [  3]  3.0- 4.0 sec   895 MBytes  7.51 Gbits/sec
  [  5]  3.0- 4.0 sec   836 MBytes  7.01 Gbits/sec
  [  4]  3.0- 4.0 sec   802 MBytes  6.72 Gbits/sec
  [SUM]  3.0- 4.0 sec  7.61 GBytes  65.4 Gbits/sec
  [ 10]  4.0- 5.0 sec  1.84 GBytes  15.8 Gbits/sec
  [  4]  4.0- 5.0 sec   381 MBytes  3.20 Gbits/sec
  [  5]  4.0- 5.0 sec   362 MBytes  3.04 Gbits/sec
  [  6]  4.0- 5.0 sec  1.66 GBytes  14.3 Gbits/sec
  [  7]  4.0- 5.0 sec  1.02 GBytes  8.79 Gbits/sec
  [  9]  4.0- 5.0 sec   434 MBytes  3.64 Gbits/sec
  [  8]  4.0- 5.0 sec  1.19 GBytes  10.2 Gbits/sec
  [  3]  4.0- 5.0 sec  1.06 GBytes  9.09 Gbits/sec
  [SUM]  4.0- 5.0 sec  7.92 GBytes  68.0 Gbits/sec
  [  8]  5.0- 6.0 sec  1.06 GBytes  9.09 Gbits/sec
  [  6]  5.0- 6.0 sec  1.51 GBytes  12.9 Gbits/sec
  [  3]  5.0- 6.0 sec   917 MBytes  7.69 Gbits/sec
  [ 10]  5.0- 6.0 sec  1.25 GBytes  10.8 Gbits/sec
  [  4]  5.0- 6.0 sec   355 MBytes  2.98 Gbits/sec
  [  7]  5.0- 6.0 sec  1.12 GBytes  9.64 Gbits/sec
  [  5]  5.0- 6.0 sec   424 MBytes  3.56 Gbits/sec
  [  9]  5.0- 6.0 sec   559 MBytes  4.69 Gbits/sec
  [SUM]  5.0- 6.0 sec  7.14 GBytes  61.4 Gbits/sec
  [  8]  6.0- 7.0 sec   995 MBytes  8.35 Gbits/sec
  [  7]  6.0- 7.0 sec  1.21 GBytes  10.4 Gbits/sec
  [  9]  6.0- 7.0 sec   339 MBytes  2.85 Gbits/sec
  [  6]  6.0- 7.0 sec  1.47 GBytes  12.6 Gbits/sec
  [  3]  6.0- 7.0 sec   927 MBytes  7.78 Gbits/sec
  [ 10]  6.0- 7.0 sec  1.61 GBytes  13.8 Gbits/sec
  [  4]  6.0- 7.0 sec   517 MBytes  4.34 Gbits/sec
  [  5]  6.0- 7.0 sec   537 MBytes  4.50 Gbits/sec
  [SUM]  6.0- 7.0 sec  7.52 GBytes  64.6 Gbits/sec
  [  8]  7.0- 8.0 sec   952 MBytes  7.99 Gbits/sec
  [  7]  7.0- 8.0 sec   966 MBytes  8.10 Gbits/sec
  [  6]  7.0- 8.0 sec  1.44 GBytes  12.4 Gbits/sec
  [ 10]  7.0- 8.0 sec  1.19 GBytes  10.2 Gbits/sec
  [  3]  7.0- 8.0 sec  1.20 GBytes  10.3 Gbits/sec
  [  9]  7.0- 8.0 sec   511 MBytes  4.29 Gbits/sec
  [  4]  7.0- 8.0 sec   539 MBytes  4.52 Gbits/sec
  [  5]  7.0- 8.0 sec   545 MBytes  4.58 Gbits/sec
  [SUM]  7.0- 8.0 sec  7.27 GBytes  62.4 Gbits/sec
  [  6]  8.0- 9.0 sec  2.03 GBytes  17.5 Gbits/sec
  [  3]  8.0- 9.0 sec   870 MBytes  7.30 Gbits/sec
  [ 10]  8.0- 9.0 sec  1.58 GBytes  13.5 Gbits/sec
  [  4]  8.0- 9.0 sec   445 MBytes  3.74 Gbits/sec
  [  7]  8.0- 9.0 sec   729 MBytes  6.11 Gbits/sec
  [  5]  8.0- 9.0 sec   237 MBytes  1.99 Gbits/sec
  [  8]  8.0- 9.0 sec  1.15 GBytes  9.88 Gbits/sec
  [  9]  8.0- 9.0 sec   508 MBytes  4.26 Gbits/sec
  [SUM]  8.0- 9.0 sec  7.48 GBytes  64.3 Gbits/sec
  [  8]  9.0-10.0 sec   746 MBytes  6.26 Gbits/sec
  [  6]  9.0-10.0 sec  1.80 GBytes  15.4 Gbits/sec
  [ 10]  9.0-10.0 sec  1.82 GBytes  15.6 Gbits/sec
  [  9]  9.0-10.0 sec   395 MBytes  3.31 Gbits/sec
  [  4]  9.0-10.0 sec   224 MBytes  1.88 Gbits/sec
  [  3]  9.0-10.0 sec   761 MBytes  6.38 Gbits/sec
  [  7]  9.0-10.0 sec   978 MBytes  8.21 Gbits/sec
  [  5]  9.0-10.0 sec   421 MBytes  3.53 Gbits/sec
  [SUM]  9.0-10.0 sec  7.06 GBytes  60.7 Gbits/sec
  [  7] 10.0-11.0 sec  1.05 GBytes  9.03 Gbits/sec
  [  6] 10.0-11.0 sec  1.19 GBytes  10.2 Gbits/sec
  [  3] 10.0-11.0 sec  1.02 GBytes  8.74 Gbits/sec
  [ 10] 10.0-11.0 sec  1.13 GBytes  9.73 Gbits/sec
  [  8] 10.0-11.0 sec   903 MBytes  7.58 Gbits/sec
  [  9] 10.0-11.0 sec   313 MBytes  2.63 Gbits/sec
  [  4] 10.0-11.0 sec   466 MBytes  3.91 Gbits/sec
  [  5] 10.0-11.0 sec   476 MBytes  3.99 Gbits/sec
  [SUM] 10.0-11.0 sec  6.50 GBytes  55.8 Gbits/sec
  [  8] 11.0-12.0 sec  1.15 GBytes  9.87 Gbits/sec
  [  7] 11.0-12.0 sec   702 MBytes  5.89 Gbits/sec
  [ 10] 11.0-12.0 sec  1.02 GBytes  8.78 Gbits/sec
  [  5] 11.0-12.0 sec   555 MBytes  4.65 Gbits/sec
  [  3] 11.0-12.0 sec   808 MBytes  6.78 Gbits/sec
  [  6] 11.0-12.0 sec  1.67 GBytes  14.4 Gbits/sec
  [  9] 11.0-12.0 sec   550 MBytes  4.62 Gbits/sec
  [  4] 11.0-12.0 sec   576 MBytes  4.83 Gbits/sec
  [SUM] 11.0-12.0 sec  6.96 GBytes  59.8 Gbits/sec
  [  8] 12.0-13.0 sec   698 MBytes  5.86 Gbits/sec
  [  6] 12.0-13.0 sec  1.43 GBytes  12.3 Gbits/sec
  [  3] 12.0-13.0 sec  1019 MBytes  8.55 Gbits/sec
  [  4] 12.0-13.0 sec   342 MBytes  2.87 Gbits/sec
  [  5] 12.0-13.0 sec   373 MBytes  3.13 Gbits/sec
  [ 10] 12.0-13.0 sec  1.34 GBytes  11.5 Gbits/sec
  [  7] 12.0-13.0 sec  1.11 GBytes  9.51 Gbits/sec
  [  9] 12.0-13.0 sec   676 MBytes  5.67 Gbits/sec
  [SUM] 12.0-13.0 sec  6.92 GBytes  59.4 Gbits/sec
  [  7] 13.0-14.0 sec   944 MBytes  7.92 Gbits/sec
  [  6] 13.0-14.0 sec  1.16 GBytes  9.97 Gbits/sec
  [ 10] 13.0-14.0 sec  1.12 GBytes  9.65 Gbits/sec
  [  9] 13.0-14.0 sec   366 MBytes  3.07 Gbits/sec
  [  3] 13.0-14.0 sec   936 MBytes  7.85 Gbits/sec
  [  8] 13.0-14.0 sec  1.09 GBytes  9.36 Gbits/sec
  [  4] 13.0-14.0 sec   694 MBytes  5.83 Gbits/sec
  [  5] 13.0-14.0 sec   726 MBytes  6.09 Gbits/sec
  [SUM] 13.0-14.0 sec  6.95 GBytes  59.7 Gbits/sec
  [  6] 14.0-15.0 sec  1.53 GBytes  13.1 Gbits/sec
  [  3] 14.0-15.0 sec  1.07 GBytes  9.16 Gbits/sec
  [ 10] 14.0-15.0 sec  1.05 GBytes  9.05 Gbits/sec
  [  5] 14.0-15.0 sec   541 MBytes  4.54 Gbits/sec
  [  7] 14.0-15.0 sec   854 MBytes  7.16 Gbits/sec
  [  9] 14.0-15.0 sec   787 MBytes  6.60 Gbits/sec
  [  8] 14.0-15.0 sec   990 MBytes  8.30 Gbits/sec
  [  4] 14.0-15.0 sec   751 MBytes  6.30 Gbits/sec
  [SUM] 14.0-15.0 sec  7.48 GBytes  64.2 Gbits/sec
  [  6] 15.0-16.0 sec  1.54 GBytes  13.3 Gbits/sec
  [ 10] 15.0-16.0 sec  1.72 GBytes  14.8 Gbits/sec
  [  9] 15.0-16.0 sec   601 MBytes  5.04 Gbits/sec
  [  4] 15.0-16.0 sec   315 MBytes  2.64 Gbits/sec
  [  8] 15.0-16.0 sec   554 MBytes  4.65 Gbits/sec
  [  3] 15.0-16.0 sec   530 MBytes  4.44 Gbits/sec
  [  5] 15.0-16.0 sec   493 MBytes  4.13 Gbits/sec
  [  7] 15.0-16.0 sec   949 MBytes  7.96 Gbits/sec
  [SUM] 15.0-16.0 sec  6.62 GBytes  56.9 Gbits/sec
  [  7] 16.0-17.0 sec   542 MBytes  4.55 Gbits/sec
  [  6] 16.0-17.0 sec  1.70 GBytes  14.6 Gbits/sec
  [ 10] 16.0-17.0 sec  1.33 GBytes  11.4 Gbits/sec
  [  9] 16.0-17.0 sec   349 MBytes  2.93 Gbits/sec
  [  4] 16.0-17.0 sec   656 MBytes  5.50 Gbits/sec
  [  8] 16.0-17.0 sec   974 MBytes  8.17 Gbits/sec
  [  3] 16.0-17.0 sec   585 MBytes  4.91 Gbits/sec
  [  5] 16.0-17.0 sec   683 MBytes  5.73 Gbits/sec
  [SUM] 16.0-17.0 sec  6.72 GBytes  57.8 Gbits/sec
  [  8] 17.0-18.0 sec   521 MBytes  4.37 Gbits/sec
  [  3] 17.0-18.0 sec   283 MBytes  2.38 Gbits/sec
  [ 10] 17.0-18.0 sec  1.86 GBytes  15.9 Gbits/sec
  [  5] 17.0-18.0 sec   378 MBytes  3.17 Gbits/sec
  [  7] 17.0-18.0 sec   850 MBytes  7.13 Gbits/sec
  [  6] 17.0-18.0 sec  1.70 GBytes  14.6 Gbits/sec
  [  9] 17.0-18.0 sec   799 MBytes  6.70 Gbits/sec
  [  4] 17.0-18.0 sec   724 MBytes  6.07 Gbits/sec
  [SUM] 17.0-18.0 sec  7.03 GBytes  60.4 Gbits/sec
  [  6] 18.0-19.0 sec  1.36 GBytes  11.7 Gbits/sec
  [ 10] 18.0-19.0 sec  1.34 GBytes  11.5 Gbits/sec
  [  9] 18.0-19.0 sec   452 MBytes  3.79 Gbits/sec
  [  4] 18.0-19.0 sec   431 MBytes  3.61 Gbits/sec
  [  3] 18.0-19.0 sec   527 MBytes  4.42 Gbits/sec
  [  5] 18.0-19.0 sec   828 MBytes  6.95 Gbits/sec
  [  8] 18.0-19.0 sec  1.15 GBytes  9.87 Gbits/sec
  [  7] 18.0-19.0 sec  1.13 GBytes  9.71 Gbits/sec
  [SUM] 18.0-19.0 sec  7.16 GBytes  61.5 Gbits/sec
  [ 10] 19.0-20.0 sec  1.69 GBytes  14.5 Gbits/sec
  [  5] 19.0-20.0 sec   501 MBytes  4.20 Gbits/sec
  [  8] 19.0-20.0 sec   447 MBytes  3.75 Gbits/sec
  [  7] 19.0-20.0 sec   446 MBytes  3.74 Gbits/sec
  [  4] 19.0-20.0 sec   985 MBytes  8.26 Gbits/sec
  [  6] 19.0-20.0 sec  1.68 GBytes  14.5 Gbits/sec
  [  9] 19.0-20.0 sec   655 MBytes  5.49 Gbits/sec
  [  3] 19.0-20.0 sec   666 MBytes  5.58 Gbits/sec
  [SUM] 19.0-20.0 sec  6.98 GBytes  60.0 Gbits/sec
  [  6] 20.0-21.0 sec  1.16 GBytes  9.93 Gbits/sec
  [ 10] 20.0-21.0 sec  1.11 GBytes  9.58 Gbits/sec
  [  9] 20.0-21.0 sec   470 MBytes  3.94 Gbits/sec
  [  4] 20.0-21.0 sec  1.18 GBytes  10.1 Gbits/sec
  [  3] 20.0-21.0 sec   453 MBytes  3.80 Gbits/sec
  [  5] 20.0-21.0 sec   304 MBytes  2.55 Gbits/sec
  [  7] 20.0-21.0 sec   766 MBytes  6.43 Gbits/sec
  [  8] 20.0-21.0 sec   868 MBytes  7.28 Gbits/sec
  [SUM] 20.0-21.0 sec  6.24 GBytes  53.6 Gbits/sec
  [  6] 21.0-22.0 sec  1.56 GBytes  13.4 Gbits/sec
  [  3] 21.0-22.0 sec  1.11 GBytes  9.57 Gbits/sec
  [  4] 21.0-22.0 sec  1.08 GBytes  9.25 Gbits/sec
  [  7] 21.0-22.0 sec   404 MBytes  3.39 Gbits/sec
  [  9] 21.0-22.0 sec   331 MBytes  2.78 Gbits/sec
  [  8] 21.0-22.0 sec   436 MBytes  3.66 Gbits/sec
  [ 10] 21.0-22.0 sec  1.57 GBytes  13.4 Gbits/sec
  [  5] 21.0-22.0 sec   537 MBytes  4.51 Gbits/sec
  [SUM] 21.0-22.0 sec  6.98 GBytes  60.0 Gbits/sec
  [  4] 22.0-23.0 sec  1.00 GBytes  8.62 Gbits/sec
  [  3] 22.0-23.0 sec   774 MBytes  6.49 Gbits/sec
  [ 10] 22.0-23.0 sec  1.04 GBytes  8.94 Gbits/sec
  [  6] 22.0-23.0 sec  1.19 GBytes  10.2 Gbits/sec
  [  9] 22.0-23.0 sec   609 MBytes  5.11 Gbits/sec
  [  7] 22.0-23.0 sec   871 MBytes  7.31 Gbits/sec
  [  8] 22.0-23.0 sec   910 MBytes  7.64 Gbits/sec
  [  5] 22.0-23.0 sec   614 MBytes  5.15 Gbits/sec
  [SUM] 22.0-23.0 sec  6.92 GBytes  59.5 Gbits/sec
  [  6] 23.0-24.0 sec  1.35 GBytes  11.6 Gbits/sec
  [  3] 23.0-24.0 sec  1.33 GBytes  11.5 Gbits/sec
  [ 10] 23.0-24.0 sec  1.17 GBytes  10.1 Gbits/sec
  [  9] 23.0-24.0 sec   450 MBytes  3.77 Gbits/sec
  [  4] 23.0-24.0 sec  1.17 GBytes  10.1 Gbits/sec
  [  7] 23.0-24.0 sec   423 MBytes  3.55 Gbits/sec
  [  5] 23.0-24.0 sec   327 MBytes  2.74 Gbits/sec
  [  8] 23.0-24.0 sec   466 MBytes  3.91 Gbits/sec
  [SUM] 23.0-24.0 sec  6.65 GBytes  57.2 Gbits/sec
  [  6] 24.0-25.0 sec  1.15 GBytes  9.91 Gbits/sec
  [  3] 24.0-25.0 sec  1.03 GBytes  8.87 Gbits/sec
  [ 10] 24.0-25.0 sec   875 MBytes  7.34 Gbits/sec
  [  4] 24.0-25.0 sec   980 MBytes  8.22 Gbits/sec
  [  7] 24.0-25.0 sec   993 MBytes  8.33 Gbits/sec
  [  5] 24.0-25.0 sec   806 MBytes  6.76 Gbits/sec
  [  9] 24.0-25.0 sec   624 MBytes  5.23 Gbits/sec
  [  8] 24.0-25.0 sec   725 MBytes  6.08 Gbits/sec
  [SUM] 24.0-25.0 sec  7.07 GBytes  60.8 Gbits/sec
  [  7] 25.0-26.0 sec   792 MBytes  6.64 Gbits/sec
  [  6] 25.0-26.0 sec  1.05 GBytes  9.00 Gbits/sec
  [  3] 25.0-26.0 sec  1.51 GBytes  12.9 Gbits/sec
  [  9] 25.0-26.0 sec   314 MBytes  2.63 Gbits/sec
  [  5] 25.0-26.0 sec   828 MBytes  6.95 Gbits/sec
  [  8] 25.0-26.0 sec   238 MBytes  1.99 Gbits/sec
  [ 10] 25.0-26.0 sec   971 MBytes  8.15 Gbits/sec
  [  4] 25.0-26.0 sec   968 MBytes  8.12 Gbits/sec
  [SUM] 25.0-26.0 sec  6.57 GBytes  56.4 Gbits/sec
  [  3] 26.0-27.0 sec  1.63 GBytes  14.0 Gbits/sec
  [  5] 26.0-27.0 sec   626 MBytes  5.26 Gbits/sec
  [  9] 26.0-27.0 sec   335 MBytes  2.81 Gbits/sec
  [  4] 26.0-27.0 sec   453 MBytes  3.80 Gbits/sec
  [  6] 26.0-27.0 sec  1.35 GBytes  11.6 Gbits/sec
  [  7] 26.0-27.0 sec   996 MBytes  8.35 Gbits/sec
  [  8] 26.0-27.0 sec   412 MBytes  3.46 Gbits/sec
  [ 10] 26.0-27.0 sec  1012 MBytes  8.49 Gbits/sec
  [SUM] 26.0-27.0 sec  6.72 GBytes  57.7 Gbits/sec
  [  7] 27.0-28.0 sec   785 MBytes  6.59 Gbits/sec
  [ 10] 27.0-28.0 sec   720 MBytes  6.04 Gbits/sec
  [  3] 27.0-28.0 sec  1.10 GBytes  9.49 Gbits/sec
  [  6] 27.0-28.0 sec  1.32 GBytes  11.3 Gbits/sec
  [  9] 27.0-28.0 sec   568 MBytes  4.76 Gbits/sec
  [  5] 27.0-28.0 sec   932 MBytes  7.82 Gbits/sec
  [  4] 27.0-28.0 sec   664 MBytes  5.57 Gbits/sec
  [  8] 27.0-28.0 sec   333 MBytes  2.79 Gbits/sec
  [SUM] 27.0-28.0 sec  6.33 GBytes  54.4 Gbits/sec
  [  6] 28.0-29.0 sec  1.17 GBytes  10.1 Gbits/sec
  [  3] 28.0-29.0 sec  1.33 GBytes  11.4 Gbits/sec
  [  9] 28.0-29.0 sec   591 MBytes  4.96 Gbits/sec
  [  4] 28.0-29.0 sec   524 MBytes  4.39 Gbits/sec
  [  5] 28.0-29.0 sec   758 MBytes  6.36 Gbits/sec
  [  7] 28.0-29.0 sec   827 MBytes  6.93 Gbits/sec
  [ 10] 28.0-29.0 sec   960 MBytes  8.06 Gbits/sec
  [  8] 28.0-29.0 sec   350 MBytes  2.93 Gbits/sec
  [SUM] 28.0-29.0 sec  6.42 GBytes  55.1 Gbits/sec
  [  6] 29.0-30.0 sec   774 MBytes  6.49 Gbits/sec
  [  3] 29.0-30.0 sec  1.26 GBytes  10.8 Gbits/sec
  [  8] 29.0-30.0 sec   192 MBytes  1.61 Gbits/sec
  [  9] 29.0-30.0 sec   723 MBytes  6.06 Gbits/sec
  [  5] 29.0-30.0 sec   596 MBytes  5.00 Gbits/sec
  [  4] 29.0-30.0 sec   460 MBytes  3.86 Gbits/sec
  [ 10] 29.0-30.0 sec  1.32 GBytes  11.4 Gbits/sec
  [  7] 29.0-30.0 sec  1.02 GBytes  8.74 Gbits/sec
  [SUM] 29.0-30.0 sec  6.28 GBytes  53.9 Gbits/sec
  [  3] 30.0-31.0 sec  1.02 GBytes  8.74 Gbits/sec
  [ 10] 30.0-31.0 sec  1.26 GBytes  10.8 Gbits/sec
  [  6] 30.0-31.0 sec  1011 MBytes  8.48 Gbits/sec
  [  9] 30.0-31.0 sec  1005 MBytes  8.43 Gbits/sec
  [  8] 30.0-31.0 sec   434 MBytes  3.64 Gbits/sec
  [  5] 30.0-31.0 sec   908 MBytes  7.61 Gbits/sec
  [  7] 30.0-31.0 sec   978 MBytes  8.20 Gbits/sec
  [  4] 30.0-31.0 sec   708 MBytes  5.94 Gbits/sec
  [SUM] 30.0-31.0 sec  7.20 GBytes  61.9 Gbits/sec
  [  7] 31.0-32.0 sec   583 MBytes  4.89 Gbits/sec
  [  9] 31.0-32.0 sec   801 MBytes  6.72 Gbits/sec
  [  4] 31.0-32.0 sec   421 MBytes  3.53 Gbits/sec
  [  5] 31.0-32.0 sec   684 MBytes  5.74 Gbits/sec
  [ 10] 31.0-32.0 sec  1.25 GBytes  10.8 Gbits/sec
  [  3] 31.0-32.0 sec  1.20 GBytes  10.3 Gbits/sec
  [  6] 31.0-32.0 sec  1.05 GBytes  9.04 Gbits/sec
  [  8] 31.0-32.0 sec   501 MBytes  4.20 Gbits/sec
  [SUM] 31.0-32.0 sec  6.43 GBytes  55.2 Gbits/sec
  [ 10] 32.0-33.0 sec  1.23 GBytes  10.6 Gbits/sec
  [  8] 32.0-33.0 sec   288 MBytes  2.42 Gbits/sec
  [  5] 32.0-33.0 sec   709 MBytes  5.95 Gbits/sec
  [  9] 32.0-33.0 sec  1015 MBytes  8.52 Gbits/sec
  [  6] 32.0-33.0 sec  1.02 GBytes  8.80 Gbits/sec
  [  4] 32.0-33.0 sec   740 MBytes  6.21 Gbits/sec
  [  7] 32.0-33.0 sec  1.01 GBytes  8.69 Gbits/sec
  [  3] 32.0-33.0 sec  1.07 GBytes  9.21 Gbits/sec
  [SUM] 32.0-33.0 sec  7.03 GBytes  60.4 Gbits/sec
  [  7] 33.0-34.0 sec   876 MBytes  7.35 Gbits/sec
  [  3] 33.0-34.0 sec   749 MBytes  6.28 Gbits/sec
  [ 10] 33.0-34.0 sec  1.77 GBytes  15.2 Gbits/sec
  [  9] 33.0-34.0 sec   780 MBytes  6.54 Gbits/sec
  [  6] 33.0-34.0 sec  1.11 GBytes  9.54 Gbits/sec
  [  8] 33.0-34.0 sec   527 MBytes  4.42 Gbits/sec
  [  5] 33.0-34.0 sec   816 MBytes  6.85 Gbits/sec
  [  4] 33.0-34.0 sec   665 MBytes  5.58 Gbits/sec
  [SUM] 33.0-34.0 sec  7.19 GBytes  61.8 Gbits/sec
  [  4] 34.0-35.0 sec   319 MBytes  2.68 Gbits/sec
  [  3] 34.0-35.0 sec   745 MBytes  6.25 Gbits/sec
  [  5] 34.0-35.0 sec   399 MBytes  3.35 Gbits/sec
  [  7] 34.0-35.0 sec   554 MBytes  4.65 Gbits/sec
  [  6] 34.0-35.0 sec  1.22 GBytes  10.5 Gbits/sec
  [ 10] 34.0-35.0 sec  1.92 GBytes  16.5 Gbits/sec
  [  9] 34.0-35.0 sec   781 MBytes  6.55 Gbits/sec
  [  8] 34.0-35.0 sec   464 MBytes  3.89 Gbits/sec
  [SUM] 34.0-35.0 sec  6.33 GBytes  54.4 Gbits/sec
  [  8] 35.0-36.0 sec   235 MBytes  1.97 Gbits/sec
  [  6] 35.0-36.0 sec  1.33 GBytes  11.4 Gbits/sec
  [ 10] 35.0-36.0 sec  1.19 GBytes  10.2 Gbits/sec
  [  7] 35.0-36.0 sec   912 MBytes  7.65 Gbits/sec
  [  5] 35.0-36.0 sec   429 MBytes  3.60 Gbits/sec
  [  4] 35.0-36.0 sec   699 MBytes  5.86 Gbits/sec
  [  3] 35.0-36.0 sec  1.10 GBytes  9.45 Gbits/sec
  [  9] 35.0-36.0 sec   479 MBytes  4.02 Gbits/sec
  [SUM] 35.0-36.0 sec  6.31 GBytes  54.2 Gbits/sec
  [  6] 36.0-37.0 sec  1.63 GBytes  14.0 Gbits/sec
  [  3] 36.0-37.0 sec   961 MBytes  8.06 Gbits/sec
  [ 10] 36.0-37.0 sec  1.15 GBytes  9.89 Gbits/sec
  [  4] 36.0-37.0 sec   844 MBytes  7.08 Gbits/sec
  [  9] 36.0-37.0 sec   286 MBytes  2.40 Gbits/sec
  [  8] 36.0-37.0 sec   287 MBytes  2.41 Gbits/sec
  [  5] 36.0-37.0 sec   424 MBytes  3.55 Gbits/sec
  [  7] 36.0-37.0 sec  1.08 GBytes  9.26 Gbits/sec
  [SUM] 36.0-37.0 sec  6.60 GBytes  56.7 Gbits/sec
  [  7] 37.0-38.0 sec   765 MBytes  6.42 Gbits/sec
  [  6] 37.0-38.0 sec  1.58 GBytes  13.5 Gbits/sec
  [ 10] 37.0-38.0 sec   938 MBytes  7.87 Gbits/sec
  [  4] 37.0-38.0 sec   528 MBytes  4.42 Gbits/sec
  [  3] 37.0-38.0 sec  1.01 GBytes  8.69 Gbits/sec
  [  8] 37.0-38.0 sec   379 MBytes  3.18 Gbits/sec
  [  9] 37.0-38.0 sec   598 MBytes  5.02 Gbits/sec
  [  5] 37.0-38.0 sec   500 MBytes  4.20 Gbits/sec
  [SUM] 37.0-38.0 sec  6.21 GBytes  53.4 Gbits/sec
  [  6] 38.0-39.0 sec  1.16 GBytes  10.0 Gbits/sec
  [  3] 38.0-39.0 sec  1.36 GBytes  11.7 Gbits/sec
  [  5] 38.0-39.0 sec   277 MBytes  2.32 Gbits/sec
  [  4] 38.0-39.0 sec   988 MBytes  8.29 Gbits/sec
  [  7] 38.0-39.0 sec  1.19 GBytes  10.2 Gbits/sec
  [ 10] 38.0-39.0 sec  1.43 GBytes  12.3 Gbits/sec
  [  8] 38.0-39.0 sec   445 MBytes  3.73 Gbits/sec
  [  9] 38.0-39.0 sec   654 MBytes  5.49 Gbits/sec
  [SUM] 38.0-39.0 sec  7.45 GBytes  64.0 Gbits/sec
  [  7] 39.0-40.0 sec   840 MBytes  7.05 Gbits/sec
  [  7]  0.0-40.0 sec  34.3 GBytes  7.37 Gbits/sec
  [  7] MSS size 8948 bytes (MTU 8988 bytes, unknown interface)
  [  3] 39.0-40.0 sec  1.07 GBytes  9.17 Gbits/sec
  [  3]  0.0-40.0 sec  38.2 GBytes  8.20 Gbits/sec
  [  3] MSS size 8948 bytes (MTU 8988 bytes, unknown interface)
  [ 10] 39.0-40.0 sec  1.20 GBytes  10.3 Gbits/sec
  [ 10]  0.0-40.0 sec  53.3 GBytes  11.4 Gbits/sec
  [ 10] MSS size 8948 bytes (MTU 8988 bytes, unknown interface)
  [  9] 39.0-40.0 sec   382 MBytes  3.21 Gbits/sec
  [  9]  0.0-40.1 sec  23.1 GBytes  4.94 Gbits/sec
  [  9] MSS size 8948 bytes (MTU 8988 bytes, unknown interface)
  [  6] 39.0-40.0 sec  1.15 GBytes  9.90 Gbits/sec
  [  6]  0.0-40.1 sec  56.3 GBytes  12.1 Gbits/sec
  [  6] MSS size 8948 bytes (MTU 8988 bytes, unknown interface)
  [  5] 39.0-40.0 sec   523 MBytes  4.39 Gbits/sec
  [  5]  0.0-40.2 sec  23.1 GBytes  4.94 Gbits/sec
  [  5] MSS size 8948 bytes (MTU 8988 bytes, unknown interface)
  [  4] 39.0-40.0 sec  1.15 GBytes  9.87 Gbits/sec
  [  4]  0.0-40.2 sec  27.0 GBytes  5.77 Gbits/sec
  [  4] MSS size 8948 bytes (MTU 8988 bytes, unknown interface)
  [  8] 39.0-40.0 sec   396 MBytes  3.32 Gbits/sec
  [SUM] 39.0-40.0 sec  6.66 GBytes  57.2 Gbits/sec
  [  8]  0.0-40.5 sec  26.6 GBytes  5.65 Gbits/sec
  [  8] MSS size 8948 bytes (MTU 8988 bytes, unknown interface)
  [SUM]  0.0-40.5 sec   282 GBytes  59.8 Gbits/sec


Error from psl01-gva-100g.cern.ch:
  No error.

Diagnostics from perfsonar-bandwidth.grid.surfsara.nl:
  No diagnostics.

Error from perfsonar-bandwidth.grid.surfsara.nl:
  iperf2 returned an error: Process took too long to run.

No further runs scheduled.
