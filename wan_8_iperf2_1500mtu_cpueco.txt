[root@psl01-gva ~]# pscheduler task   --tool iperf2 throughput --source psl01-gva-100g.cern.ch --dest perfsonar-bandwidth.grid.surfsara.nl  --ip-version=4 -t 60 -i 1 --parallel 8
Submitting task...
Task URL:
https://psl01-gva-100g.cern.ch/pscheduler/tasks/a66b3595-25a1-41e3-8485-214b7ded78ae
Running with tool 'iperf2'
Fetching first run...

Next scheduled run:
https://psl01-gva-100g.cern.ch/pscheduler/tasks/a66b3595-25a1-41e3-8485-214b7ded78ae/runs/a76759b8-be5c-40f0-9b42-26be9ddf04df
Starts 2019-05-27T14:10:22+02:00 (~8 seconds)
Ends   2019-05-27T14:11:27+02:00 (~64 seconds)
Waiting for result...

Run did not complete: Failed


Diagnostics from psl01-gva-100g.cern.ch:
  /usr/bin/iperf -p 5001 -c 145.100.32.32 -t 60 -m -i 1 -P 8

  ------------------------------------------------------------
  Client connecting to 145.100.32.32, TCP port 5001
  TCP window size:  325 KByte (default)
  ------------------------------------------------------------
  [  5] local 128.142.208.137 port 54651 connected with 145.100.32.32 port 5001
  [  6] local 128.142.208.137 port 54658 connected with 145.100.32.32 port 5001
  [  8] local 128.142.208.137 port 54662 connected with 145.100.32.32 port 5001
  [ 10] local 128.142.208.137 port 54666 connected with 145.100.32.32 port 5001
  [  7] local 128.142.208.137 port 54660 connected with 145.100.32.32 port 5001
  [  9] local 128.142.208.137 port 54664 connected with 145.100.32.32 port 5001
  [  4] local 128.142.208.137 port 54652 connected with 145.100.32.32 port 5001
  [  3] local 128.142.208.137 port 54650 connected with 145.100.32.32 port 5001
  [ ID] Interval       Transfer     Bandwidth
  [  5]  0.0- 1.0 sec  1.04 GBytes  8.96 Gbits/sec
  [  4]  0.0- 1.0 sec  1.00 GBytes  8.61 Gbits/sec
  [  3]  0.0- 1.0 sec   520 MBytes  4.36 Gbits/sec
  [  8]  0.0- 1.0 sec   639 MBytes  5.36 Gbits/sec
  [  7]  0.0- 1.0 sec   979 MBytes  8.21 Gbits/sec
  [  6]  0.0- 1.0 sec   668 MBytes  5.61 Gbits/sec
  [  9]  0.0- 1.0 sec   479 MBytes  4.01 Gbits/sec
  [ 10]  0.0- 1.0 sec   490 MBytes  4.11 Gbits/sec
  [SUM]  0.0- 1.0 sec  5.73 GBytes  49.2 Gbits/sec
  [  5]  1.0- 2.0 sec  1.32 GBytes  11.4 Gbits/sec
  [  4]  1.0- 2.0 sec   828 MBytes  6.94 Gbits/sec
  [  7]  1.0- 2.0 sec   759 MBytes  6.37 Gbits/sec
  [  9]  1.0- 2.0 sec   271 MBytes  2.28 Gbits/sec
  [ 10]  1.0- 2.0 sec   265 MBytes  2.23 Gbits/sec
  [  3]  1.0- 2.0 sec   249 MBytes  2.09 Gbits/sec
  [  6]  1.0- 2.0 sec   498 MBytes  4.18 Gbits/sec
  [  8]  1.0- 2.0 sec   444 MBytes  3.72 Gbits/sec
  [SUM]  1.0- 2.0 sec  4.56 GBytes  39.2 Gbits/sec
  [  5]  2.0- 3.0 sec  1.07 GBytes  9.23 Gbits/sec
  [  7]  2.0- 3.0 sec   800 MBytes  6.71 Gbits/sec
  [  4]  2.0- 3.0 sec   844 MBytes  7.08 Gbits/sec
  [  6]  2.0- 3.0 sec   495 MBytes  4.16 Gbits/sec
  [  3]  2.0- 3.0 sec   318 MBytes  2.67 Gbits/sec
  [  8]  2.0- 3.0 sec   339 MBytes  2.84 Gbits/sec
  [  9]  2.0- 3.0 sec   327 MBytes  2.74 Gbits/sec
  [ 10]  2.0- 3.0 sec   361 MBytes  3.03 Gbits/sec
  [SUM]  2.0- 3.0 sec  4.48 GBytes  38.5 Gbits/sec
  [  5]  3.0- 4.0 sec  1.15 GBytes  9.85 Gbits/sec
  [  6]  3.0- 4.0 sec   575 MBytes  4.83 Gbits/sec
  [  7]  3.0- 4.0 sec   787 MBytes  6.60 Gbits/sec
  [  4]  3.0- 4.0 sec   886 MBytes  7.43 Gbits/sec
  [  3]  3.0- 4.0 sec   333 MBytes  2.79 Gbits/sec
  [  9]  3.0- 4.0 sec   324 MBytes  2.72 Gbits/sec
  [  8]  3.0- 4.0 sec   453 MBytes  3.80 Gbits/sec
  [ 10]  3.0- 4.0 sec   354 MBytes  2.97 Gbits/sec
  [SUM]  3.0- 4.0 sec  4.77 GBytes  41.0 Gbits/sec
  [  5]  4.0- 5.0 sec  1.03 GBytes  8.81 Gbits/sec
  [  7]  4.0- 5.0 sec   881 MBytes  7.39 Gbits/sec
  [  3]  4.0- 5.0 sec   405 MBytes  3.39 Gbits/sec
  [ 10]  4.0- 5.0 sec   279 MBytes  2.34 Gbits/sec
  [  8]  4.0- 5.0 sec   352 MBytes  2.95 Gbits/sec
  [  9]  4.0- 5.0 sec   340 MBytes  2.85 Gbits/sec
  [  6]  4.0- 5.0 sec   547 MBytes  4.59 Gbits/sec
  [  4]  4.0- 5.0 sec   841 MBytes  7.05 Gbits/sec
  [SUM]  4.0- 5.0 sec  4.58 GBytes  39.4 Gbits/sec
  [  5]  5.0- 6.0 sec  1.01 GBytes  8.66 Gbits/sec
  [ 10]  5.0- 6.0 sec   424 MBytes  3.56 Gbits/sec
  [  9]  5.0- 6.0 sec   308 MBytes  2.58 Gbits/sec
  [  6]  5.0- 6.0 sec   499 MBytes  4.19 Gbits/sec
  [  8]  5.0- 6.0 sec   466 MBytes  3.91 Gbits/sec
  [  7]  5.0- 6.0 sec   892 MBytes  7.48 Gbits/sec
  [  3]  5.0- 6.0 sec   407 MBytes  3.42 Gbits/sec
  [  4]  5.0- 6.0 sec   443 MBytes  3.72 Gbits/sec
  [SUM]  5.0- 6.0 sec  4.37 GBytes  37.5 Gbits/sec
  [  5]  6.0- 7.0 sec  1019 MBytes  8.55 Gbits/sec
  [  3]  6.0- 7.0 sec   342 MBytes  2.87 Gbits/sec
  [  9]  6.0- 7.0 sec   380 MBytes  3.19 Gbits/sec
  [  6]  6.0- 7.0 sec   666 MBytes  5.59 Gbits/sec
  [ 10]  6.0- 7.0 sec   420 MBytes  3.53 Gbits/sec
  [  7]  6.0- 7.0 sec   892 MBytes  7.48 Gbits/sec
  [  8]  6.0- 7.0 sec   487 MBytes  4.09 Gbits/sec
  [  4]  6.0- 7.0 sec   444 MBytes  3.72 Gbits/sec
  [SUM]  6.0- 7.0 sec  4.54 GBytes  39.0 Gbits/sec
  [  5]  7.0- 8.0 sec  1.11 GBytes  9.50 Gbits/sec
  [ 10]  7.0- 8.0 sec   455 MBytes  3.82 Gbits/sec
  [  7]  7.0- 8.0 sec   921 MBytes  7.72 Gbits/sec
  [  8]  7.0- 8.0 sec   479 MBytes  4.02 Gbits/sec
  [  3]  7.0- 8.0 sec   490 MBytes  4.11 Gbits/sec
  [  6]  7.0- 8.0 sec   652 MBytes  5.47 Gbits/sec
  [  9]  7.0- 8.0 sec   422 MBytes  3.54 Gbits/sec
  [  4]  7.0- 8.0 sec   469 MBytes  3.94 Gbits/sec
  [SUM]  7.0- 8.0 sec  4.90 GBytes  42.1 Gbits/sec
  [  5]  8.0- 9.0 sec  1.16 GBytes  10.0 Gbits/sec
  [  8]  8.0- 9.0 sec   603 MBytes  5.06 Gbits/sec
  [  7]  8.0- 9.0 sec   988 MBytes  8.29 Gbits/sec
  [  9]  8.0- 9.0 sec   387 MBytes  3.25 Gbits/sec
  [  6]  8.0- 9.0 sec   666 MBytes  5.58 Gbits/sec
  [  3]  8.0- 9.0 sec   502 MBytes  4.21 Gbits/sec
  [ 10]  8.0- 9.0 sec   485 MBytes  4.07 Gbits/sec
  [  4]  8.0- 9.0 sec   415 MBytes  3.48 Gbits/sec
  [SUM]  8.0- 9.0 sec  5.12 GBytes  43.9 Gbits/sec
  [  5]  9.0-10.0 sec  1.16 GBytes  9.95 Gbits/sec
  [  6]  9.0-10.0 sec   847 MBytes  7.11 Gbits/sec
  [  9]  9.0-10.0 sec   468 MBytes  3.92 Gbits/sec
  [  4]  9.0-10.0 sec   483 MBytes  4.05 Gbits/sec
  [  8]  9.0-10.0 sec   547 MBytes  4.59 Gbits/sec
  [ 10]  9.0-10.0 sec   560 MBytes  4.70 Gbits/sec
  [  3]  9.0-10.0 sec   649 MBytes  5.45 Gbits/sec
  [  7]  9.0-10.0 sec   578 MBytes  4.85 Gbits/sec
  [SUM]  9.0-10.0 sec  5.19 GBytes  44.6 Gbits/sec
  [  5] 10.0-11.0 sec  1.08 GBytes  9.31 Gbits/sec
  [  6] 10.0-11.0 sec   788 MBytes  6.61 Gbits/sec
  [  8] 10.0-11.0 sec   717 MBytes  6.02 Gbits/sec
  [  9] 10.0-11.0 sec   508 MBytes  4.26 Gbits/sec
  [  3] 10.0-11.0 sec   549 MBytes  4.60 Gbits/sec
  [  7] 10.0-11.0 sec   503 MBytes  4.22 Gbits/sec
  [  4] 10.0-11.0 sec   597 MBytes  5.01 Gbits/sec
  [ 10] 10.0-11.0 sec   700 MBytes  5.87 Gbits/sec
  [SUM] 10.0-11.0 sec  5.34 GBytes  45.9 Gbits/sec
  [  5] 11.0-12.0 sec   951 MBytes  7.98 Gbits/sec
  [  6] 11.0-12.0 sec   986 MBytes  8.27 Gbits/sec
  [  9] 11.0-12.0 sec   550 MBytes  4.62 Gbits/sec
  [  3] 11.0-12.0 sec   328 MBytes  2.75 Gbits/sec
  [  4] 11.0-12.0 sec   501 MBytes  4.20 Gbits/sec
  [ 10] 11.0-12.0 sec   306 MBytes  2.56 Gbits/sec
  [  8] 11.0-12.0 sec   777 MBytes  6.52 Gbits/sec
  [  7] 11.0-12.0 sec   518 MBytes  4.35 Gbits/sec
  [SUM] 11.0-12.0 sec  4.80 GBytes  41.3 Gbits/sec
  [  5] 12.0-13.0 sec  1.06 GBytes  9.12 Gbits/sec
  [  6] 12.0-13.0 sec  1.09 GBytes  9.36 Gbits/sec
  [  8] 12.0-13.0 sec   775 MBytes  6.50 Gbits/sec
  [  4] 12.0-13.0 sec   689 MBytes  5.78 Gbits/sec
  [  7] 12.0-13.0 sec   490 MBytes  4.11 Gbits/sec
  [  9] 12.0-13.0 sec   639 MBytes  5.36 Gbits/sec
  [ 10] 12.0-13.0 sec   402 MBytes  3.37 Gbits/sec
  [  3] 12.0-13.0 sec   428 MBytes  3.59 Gbits/sec
  [SUM] 12.0-13.0 sec  5.49 GBytes  47.2 Gbits/sec
  [  5] 13.0-14.0 sec  1.08 GBytes  9.29 Gbits/sec
  [  6] 13.0-14.0 sec   532 MBytes  4.46 Gbits/sec
  [  9] 13.0-14.0 sec   619 MBytes  5.19 Gbits/sec
  [  8] 13.0-14.0 sec   909 MBytes  7.63 Gbits/sec
  [  7] 13.0-14.0 sec   542 MBytes  4.55 Gbits/sec
  [ 10] 13.0-14.0 sec   304 MBytes  2.55 Gbits/sec
  [  4] 13.0-14.0 sec   685 MBytes  5.75 Gbits/sec
  [  3] 13.0-14.0 sec   334 MBytes  2.80 Gbits/sec
  [SUM] 13.0-14.0 sec  4.91 GBytes  42.2 Gbits/sec
  [  5] 14.0-15.0 sec  1.12 GBytes  9.60 Gbits/sec
  [  7] 14.0-15.0 sec   604 MBytes  5.06 Gbits/sec
  [  4] 14.0-15.0 sec   733 MBytes  6.15 Gbits/sec
  [  6] 14.0-15.0 sec   542 MBytes  4.54 Gbits/sec
  [  9] 14.0-15.0 sec   743 MBytes  6.23 Gbits/sec
  [  3] 14.0-15.0 sec   337 MBytes  2.82 Gbits/sec
  [  8] 14.0-15.0 sec   942 MBytes  7.91 Gbits/sec
  [ 10] 14.0-15.0 sec   417 MBytes  3.50 Gbits/sec
  [SUM] 14.0-15.0 sec  5.33 GBytes  45.8 Gbits/sec
  [  5] 15.0-16.0 sec  1.05 GBytes  9.04 Gbits/sec
  [  6] 15.0-16.0 sec   540 MBytes  4.53 Gbits/sec
  [  7] 15.0-16.0 sec   673 MBytes  5.65 Gbits/sec
  [  9] 15.0-16.0 sec   771 MBytes  6.46 Gbits/sec
  [  3] 15.0-16.0 sec   350 MBytes  2.94 Gbits/sec
  [ 10] 15.0-16.0 sec   317 MBytes  2.66 Gbits/sec
  [  4] 15.0-16.0 sec   634 MBytes  5.32 Gbits/sec
  [  8] 15.0-16.0 sec   543 MBytes  4.55 Gbits/sec
  [SUM] 15.0-16.0 sec  4.79 GBytes  41.2 Gbits/sec
  [  5] 16.0-17.0 sec  1.17 GBytes  10.0 Gbits/sec
  [  7] 16.0-17.0 sec   553 MBytes  4.64 Gbits/sec
  [  9] 16.0-17.0 sec   859 MBytes  7.20 Gbits/sec
  [ 10] 16.0-17.0 sec   434 MBytes  3.64 Gbits/sec
  [  6] 16.0-17.0 sec   631 MBytes  5.29 Gbits/sec
  [  3] 16.0-17.0 sec   489 MBytes  4.10 Gbits/sec
  [  8] 16.0-17.0 sec   535 MBytes  4.49 Gbits/sec
  [  4] 16.0-17.0 sec   441 MBytes  3.70 Gbits/sec
  [SUM] 16.0-17.0 sec  5.02 GBytes  43.1 Gbits/sec
  [  5] 17.0-18.0 sec  1.12 GBytes  9.65 Gbits/sec
  [  6] 17.0-18.0 sec   560 MBytes  4.70 Gbits/sec
  [ 10] 17.0-18.0 sec   489 MBytes  4.10 Gbits/sec
  [  3] 17.0-18.0 sec   396 MBytes  3.32 Gbits/sec
  [  7] 17.0-18.0 sec   756 MBytes  6.34 Gbits/sec
  [  9] 17.0-18.0 sec   984 MBytes  8.25 Gbits/sec
  [  8] 17.0-18.0 sec   514 MBytes  4.32 Gbits/sec
  [  4] 17.0-18.0 sec   422 MBytes  3.54 Gbits/sec
  [SUM] 17.0-18.0 sec  5.15 GBytes  44.2 Gbits/sec
  [  5] 18.0-19.0 sec  1.04 GBytes  8.96 Gbits/sec
  [  7] 18.0-19.0 sec   812 MBytes  6.81 Gbits/sec
  [  9] 18.0-19.0 sec  1012 MBytes  8.49 Gbits/sec
  [  3] 18.0-19.0 sec   518 MBytes  4.35 Gbits/sec
  [  8] 18.0-19.0 sec   533 MBytes  4.47 Gbits/sec
  [ 10] 18.0-19.0 sec   516 MBytes  4.33 Gbits/sec
  [  6] 18.0-19.0 sec   696 MBytes  5.83 Gbits/sec
  [  4] 18.0-19.0 sec   437 MBytes  3.66 Gbits/sec
  [SUM] 18.0-19.0 sec  5.46 GBytes  46.9 Gbits/sec
  [  5] 19.0-20.0 sec   985 MBytes  8.26 Gbits/sec
  [ 10] 19.0-20.0 sec   523 MBytes  4.39 Gbits/sec
  [  9] 19.0-20.0 sec   940 MBytes  7.89 Gbits/sec
  [  8] 19.0-20.0 sec   526 MBytes  4.41 Gbits/sec
  [  7] 19.0-20.0 sec   810 MBytes  6.80 Gbits/sec
  [  6] 19.0-20.0 sec   592 MBytes  4.97 Gbits/sec
  [  3] 19.0-20.0 sec   575 MBytes  4.82 Gbits/sec
  [  4] 19.0-20.0 sec   430 MBytes  3.61 Gbits/sec
  [SUM] 19.0-20.0 sec  5.26 GBytes  45.1 Gbits/sec
  [  5] 20.0-21.0 sec  1.53 GBytes  13.2 Gbits/sec
  [  7] 20.0-21.0 sec   898 MBytes  7.53 Gbits/sec
  [ 10] 20.0-21.0 sec   643 MBytes  5.39 Gbits/sec
  [  3] 20.0-21.0 sec   626 MBytes  5.25 Gbits/sec
  [  4] 20.0-21.0 sec   445 MBytes  3.74 Gbits/sec
  [  6] 20.0-21.0 sec   792 MBytes  6.65 Gbits/sec
  [  9] 20.0-21.0 sec   892 MBytes  7.48 Gbits/sec
  [  8] 20.0-21.0 sec   730 MBytes  6.12 Gbits/sec
  [SUM] 20.0-21.0 sec  6.44 GBytes  55.3 Gbits/sec
  [  5] 21.0-22.0 sec  1.17 GBytes  10.1 Gbits/sec
  [  6] 21.0-22.0 sec   770 MBytes  6.46 Gbits/sec
  [ 10] 21.0-22.0 sec   707 MBytes  5.93 Gbits/sec
  [  4] 21.0-22.0 sec   595 MBytes  4.99 Gbits/sec
  [  3] 21.0-22.0 sec   652 MBytes  5.47 Gbits/sec
  [  9] 21.0-22.0 sec   455 MBytes  3.82 Gbits/sec
  [  7] 21.0-22.0 sec   787 MBytes  6.60 Gbits/sec
  [  8] 21.0-22.0 sec   639 MBytes  5.36 Gbits/sec
  [SUM] 21.0-22.0 sec  5.67 GBytes  48.7 Gbits/sec
  [  5] 22.0-23.0 sec  1.12 GBytes  9.58 Gbits/sec
  [  6] 22.0-23.0 sec   566 MBytes  4.75 Gbits/sec
  [  9] 22.0-23.0 sec   478 MBytes  4.01 Gbits/sec
  [  4] 22.0-23.0 sec   455 MBytes  3.82 Gbits/sec
  [  8] 22.0-23.0 sec   610 MBytes  5.12 Gbits/sec
  [  7] 22.0-23.0 sec  1.00 GBytes  8.61 Gbits/sec
  [  3] 22.0-23.0 sec   468 MBytes  3.92 Gbits/sec
  [ 10] 22.0-23.0 sec   496 MBytes  4.16 Gbits/sec
  [SUM] 22.0-23.0 sec  5.12 GBytes  44.0 Gbits/sec
  [  5] 23.0-24.0 sec  1.02 GBytes  8.77 Gbits/sec
  [  7] 23.0-24.0 sec  1.06 GBytes  9.12 Gbits/sec
  [  8] 23.0-24.0 sec   712 MBytes  5.98 Gbits/sec
  [  3] 23.0-24.0 sec   344 MBytes  2.89 Gbits/sec
  [  9] 23.0-24.0 sec   261 MBytes  2.19 Gbits/sec
  [  4] 23.0-24.0 sec   675 MBytes  5.66 Gbits/sec
  [ 10] 23.0-24.0 sec   348 MBytes  2.92 Gbits/sec
  [  6] 23.0-24.0 sec   520 MBytes  4.36 Gbits/sec
  [SUM] 23.0-24.0 sec  4.88 GBytes  41.9 Gbits/sec
  [  7] 24.0-25.0 sec   990 MBytes  8.31 Gbits/sec
  [  6] 24.0-25.0 sec   330 MBytes  2.76 Gbits/sec
  [ 10] 24.0-25.0 sec   354 MBytes  2.97 Gbits/sec
  [  4] 24.0-25.0 sec   671 MBytes  5.63 Gbits/sec
  [  5] 24.0-25.0 sec  1023 MBytes  8.58 Gbits/sec
  [  8] 24.0-25.0 sec   920 MBytes  7.72 Gbits/sec
  [  3] 24.0-25.0 sec   452 MBytes  3.79 Gbits/sec
  [  9] 24.0-25.0 sec   313 MBytes  2.62 Gbits/sec
  [SUM] 24.0-25.0 sec  4.93 GBytes  42.4 Gbits/sec
  [  7] 25.0-26.0 sec  1.05 GBytes  9.04 Gbits/sec
  [  4] 25.0-26.0 sec   733 MBytes  6.15 Gbits/sec
  [  3] 25.0-26.0 sec   352 MBytes  2.96 Gbits/sec
  [  8] 25.0-26.0 sec   674 MBytes  5.65 Gbits/sec
  [  6] 25.0-26.0 sec   491 MBytes  4.12 Gbits/sec
  [  5] 25.0-26.0 sec  1008 MBytes  8.45 Gbits/sec
  [  9] 25.0-26.0 sec   310 MBytes  2.60 Gbits/sec
  [ 10] 25.0-26.0 sec   472 MBytes  3.96 Gbits/sec
  [SUM] 25.0-26.0 sec  5.00 GBytes  42.9 Gbits/sec
  [  6] 26.0-27.0 sec   422 MBytes  3.54 Gbits/sec
  [  7] 26.0-27.0 sec  1.07 GBytes  9.23 Gbits/sec
  [  3] 26.0-27.0 sec   406 MBytes  3.41 Gbits/sec
  [  8] 26.0-27.0 sec   897 MBytes  7.52 Gbits/sec
  [ 10] 26.0-27.0 sec   259 MBytes  2.17 Gbits/sec
  [  4] 26.0-27.0 sec   861 MBytes  7.22 Gbits/sec
  [  9] 26.0-27.0 sec   256 MBytes  2.15 Gbits/sec
  [  5] 26.0-27.0 sec  1.07 GBytes  9.17 Gbits/sec
  [SUM] 26.0-27.0 sec  5.17 GBytes  44.4 Gbits/sec
  [  5] 27.0-28.0 sec   572 MBytes  4.80 Gbits/sec
  [  8] 27.0-28.0 sec   896 MBytes  7.51 Gbits/sec
  [  7] 27.0-28.0 sec  1003 MBytes  8.41 Gbits/sec
  [  4] 27.0-28.0 sec   900 MBytes  7.55 Gbits/sec
  [  6] 27.0-28.0 sec   452 MBytes  3.79 Gbits/sec
  [  3] 27.0-28.0 sec   466 MBytes  3.91 Gbits/sec
  [ 10] 27.0-28.0 sec   480 MBytes  4.03 Gbits/sec
  [  9] 27.0-28.0 sec   282 MBytes  2.37 Gbits/sec
  [SUM] 27.0-28.0 sec  4.93 GBytes  42.4 Gbits/sec
  [  8] 28.0-29.0 sec  1.04 GBytes  8.92 Gbits/sec
  [  7] 28.0-29.0 sec  1.03 GBytes  8.87 Gbits/sec
  [  4] 28.0-29.0 sec  1.08 GBytes  9.26 Gbits/sec
  [ 10] 28.0-29.0 sec   516 MBytes  4.33 Gbits/sec
  [  6] 28.0-29.0 sec   592 MBytes  4.97 Gbits/sec
  [  3] 28.0-29.0 sec   556 MBytes  4.67 Gbits/sec
  [  9] 28.0-29.0 sec   350 MBytes  2.94 Gbits/sec
  [  5] 28.0-29.0 sec  1.24 GBytes  10.6 Gbits/sec
  [SUM] 28.0-29.0 sec  6.35 GBytes  54.6 Gbits/sec
  [  5] 29.0-30.0 sec   586 MBytes  4.92 Gbits/sec
  [  7] 29.0-30.0 sec  1.30 GBytes  11.2 Gbits/sec
  [  3] 29.0-30.0 sec   380 MBytes  3.19 Gbits/sec
  [ 10] 29.0-30.0 sec   387 MBytes  3.25 Gbits/sec
  [  4] 29.0-30.0 sec   939 MBytes  7.87 Gbits/sec
  [  9] 29.0-30.0 sec   353 MBytes  2.96 Gbits/sec
  [  6] 29.0-30.0 sec   582 MBytes  4.89 Gbits/sec
  [  8] 29.0-30.0 sec  1.27 GBytes  10.9 Gbits/sec
  [SUM] 29.0-30.0 sec  5.72 GBytes  49.2 Gbits/sec
  [  5] 30.0-31.0 sec  1.57 GBytes  13.5 Gbits/sec
  [  8] 30.0-31.0 sec   632 MBytes  5.30 Gbits/sec
  [  7] 30.0-31.0 sec   722 MBytes  6.06 Gbits/sec
  [  9] 30.0-31.0 sec   424 MBytes  3.56 Gbits/sec
  [  4] 30.0-31.0 sec   589 MBytes  4.94 Gbits/sec
  [  6] 30.0-31.0 sec   608 MBytes  5.10 Gbits/sec
  [  3] 30.0-31.0 sec   339 MBytes  2.84 Gbits/sec
  [ 10] 30.0-31.0 sec   322 MBytes  2.70 Gbits/sec
  [SUM] 30.0-31.0 sec  5.13 GBytes  44.0 Gbits/sec
  [  5] 31.0-32.0 sec   332 MBytes  2.78 Gbits/sec
  [ 10] 31.0-32.0 sec   213 MBytes  1.79 Gbits/sec
  [  7] 31.0-32.0 sec   710 MBytes  5.96 Gbits/sec
  [  3] 31.0-32.0 sec   259 MBytes  2.17 Gbits/sec
  [  8] 31.0-32.0 sec   566 MBytes  4.75 Gbits/sec
  [  4] 31.0-32.0 sec   606 MBytes  5.08 Gbits/sec
  [  6] 31.0-32.0 sec   700 MBytes  5.87 Gbits/sec
  [  9] 31.0-32.0 sec   536 MBytes  4.50 Gbits/sec
  [SUM] 31.0-32.0 sec  3.83 GBytes  32.9 Gbits/sec
  [  5] 32.0-33.0 sec   907 MBytes  7.61 Gbits/sec
  [  6] 32.0-33.0 sec   712 MBytes  5.98 Gbits/sec
  [  9] 32.0-33.0 sec   552 MBytes  4.63 Gbits/sec
  [  3] 32.0-33.0 sec   280 MBytes  2.35 Gbits/sec
  [ 10] 32.0-33.0 sec   326 MBytes  2.74 Gbits/sec
  [  8] 32.0-33.0 sec   759 MBytes  6.36 Gbits/sec
  [  7] 32.0-33.0 sec   717 MBytes  6.01 Gbits/sec
  [  4] 32.0-33.0 sec   667 MBytes  5.60 Gbits/sec
  [SUM] 32.0-33.0 sec  4.81 GBytes  41.3 Gbits/sec
  [  5] 33.0-34.0 sec   813 MBytes  6.82 Gbits/sec
  [  8] 33.0-34.0 sec   573 MBytes  4.81 Gbits/sec
  [  9] 33.0-34.0 sec   602 MBytes  5.05 Gbits/sec
  [  4] 33.0-34.0 sec   544 MBytes  4.57 Gbits/sec
  [  3] 33.0-34.0 sec   325 MBytes  2.73 Gbits/sec
  [  7] 33.0-34.0 sec   677 MBytes  5.68 Gbits/sec
  [  6] 33.0-34.0 sec   782 MBytes  6.56 Gbits/sec
  [ 10] 33.0-34.0 sec   324 MBytes  2.72 Gbits/sec
  [SUM] 33.0-34.0 sec  4.53 GBytes  38.9 Gbits/sec
  [  5] 34.0-35.0 sec  1.13 GBytes  9.67 Gbits/sec
  [ 10] 34.0-35.0 sec   355 MBytes  2.98 Gbits/sec
  [  4] 34.0-35.0 sec   253 MBytes  2.12 Gbits/sec
  [  6] 34.0-35.0 sec   350 MBytes  2.93 Gbits/sec
  [  9] 34.0-35.0 sec   760 MBytes  6.38 Gbits/sec
  [  7] 34.0-35.0 sec   681 MBytes  5.71 Gbits/sec
  [  3] 34.0-35.0 sec   329 MBytes  2.76 Gbits/sec
  [  8] 34.0-35.0 sec   779 MBytes  6.53 Gbits/sec
  [SUM] 34.0-35.0 sec  4.55 GBytes  39.1 Gbits/sec
  [  5] 35.0-36.0 sec  1.13 GBytes  9.75 Gbits/sec
  [ 10] 35.0-36.0 sec   345 MBytes  2.90 Gbits/sec
  [  4] 35.0-36.0 sec   293 MBytes  2.46 Gbits/sec
  [  8] 35.0-36.0 sec   665 MBytes  5.58 Gbits/sec
  [  7] 35.0-36.0 sec   721 MBytes  6.05 Gbits/sec
  [  9] 35.0-36.0 sec   891 MBytes  7.47 Gbits/sec
  [  3] 35.0-36.0 sec   345 MBytes  2.89 Gbits/sec
  [  6] 35.0-36.0 sec   535 MBytes  4.49 Gbits/sec
  [SUM] 35.0-36.0 sec  4.84 GBytes  41.6 Gbits/sec
  [  5] 36.0-37.0 sec  1.19 GBytes  10.2 Gbits/sec
  [  7] 36.0-37.0 sec   876 MBytes  7.35 Gbits/sec
  [  4] 36.0-37.0 sec   366 MBytes  3.07 Gbits/sec
  [  3] 36.0-37.0 sec   417 MBytes  3.49 Gbits/sec
  [  6] 36.0-37.0 sec   355 MBytes  2.98 Gbits/sec
  [  9] 36.0-37.0 sec  1.02 GBytes  8.76 Gbits/sec
  [  8] 36.0-37.0 sec   878 MBytes  7.37 Gbits/sec
  [ 10] 36.0-37.0 sec   391 MBytes  3.28 Gbits/sec
  [SUM] 36.0-37.0 sec  5.42 GBytes  46.5 Gbits/sec
  [  5] 37.0-38.0 sec  1.19 GBytes  10.2 Gbits/sec
  [ 10] 37.0-38.0 sec   421 MBytes  3.53 Gbits/sec
  [  7] 37.0-38.0 sec   786 MBytes  6.59 Gbits/sec
  [  9] 37.0-38.0 sec   777 MBytes  6.52 Gbits/sec
  [  3] 37.0-38.0 sec   486 MBytes  4.08 Gbits/sec
  [  8] 37.0-38.0 sec   857 MBytes  7.19 Gbits/sec
  [  6] 37.0-38.0 sec   543 MBytes  4.56 Gbits/sec
  [  4] 37.0-38.0 sec   424 MBytes  3.56 Gbits/sec
  [SUM] 37.0-38.0 sec  5.38 GBytes  46.2 Gbits/sec
  [  5] 38.0-39.0 sec  1.19 GBytes  10.2 Gbits/sec
  [  8] 38.0-39.0 sec   850 MBytes  7.13 Gbits/sec
  [ 10] 38.0-39.0 sec   480 MBytes  4.02 Gbits/sec
  [  9] 38.0-39.0 sec  1.42 GBytes  12.2 Gbits/sec
  [  3] 38.0-39.0 sec   509 MBytes  4.27 Gbits/sec
  [  7] 38.0-39.0 sec   972 MBytes  8.15 Gbits/sec
  [  6] 38.0-39.0 sec   527 MBytes  4.42 Gbits/sec
  [  4] 38.0-39.0 sec   330 MBytes  2.77 Gbits/sec
  [SUM] 38.0-39.0 sec  6.19 GBytes  53.2 Gbits/sec
  [  5] 39.0-40.0 sec  1.10 GBytes  9.47 Gbits/sec
  [  8] 39.0-40.0 sec   939 MBytes  7.88 Gbits/sec
  [  9] 39.0-40.0 sec   856 MBytes  7.18 Gbits/sec
  [  7] 39.0-40.0 sec   988 MBytes  8.28 Gbits/sec
  [  4] 39.0-40.0 sec   359 MBytes  3.01 Gbits/sec
  [  3] 39.0-40.0 sec   462 MBytes  3.88 Gbits/sec
  [  6] 39.0-40.0 sec   520 MBytes  4.36 Gbits/sec
  [ 10] 39.0-40.0 sec   522 MBytes  4.38 Gbits/sec
  [SUM] 39.0-40.0 sec  5.64 GBytes  48.4 Gbits/sec
  [  5] 40.0-41.0 sec  1.06 GBytes  9.10 Gbits/sec
  [  8] 40.0-41.0 sec   989 MBytes  8.29 Gbits/sec
  [  7] 40.0-41.0 sec  1.06 GBytes  9.07 Gbits/sec
  [  9] 40.0-41.0 sec  1.06 GBytes  9.09 Gbits/sec
  [  4] 40.0-41.0 sec   373 MBytes  3.13 Gbits/sec
  [  3] 40.0-41.0 sec   316 MBytes  2.65 Gbits/sec
  [ 10] 40.0-41.0 sec   226 MBytes  1.90 Gbits/sec
  [  6] 40.0-41.0 sec   567 MBytes  4.75 Gbits/sec
  [SUM] 40.0-41.0 sec  5.58 GBytes  48.0 Gbits/sec
  [  5] 41.0-42.0 sec  1.07 GBytes  9.21 Gbits/sec
  [  8] 41.0-42.0 sec  1.16 GBytes  9.92 Gbits/sec
  [  9] 41.0-42.0 sec  1.07 GBytes  9.18 Gbits/sec
  [  3] 41.0-42.0 sec   235 MBytes  1.97 Gbits/sec
  [  7] 41.0-42.0 sec   657 MBytes  5.51 Gbits/sec
  [ 10] 41.0-42.0 sec   335 MBytes  2.81 Gbits/sec
  [  6] 41.0-42.0 sec   602 MBytes  5.05 Gbits/sec
  [  4] 41.0-42.0 sec   614 MBytes  5.15 Gbits/sec
  [SUM] 41.0-42.0 sec  5.68 GBytes  48.8 Gbits/sec
  [  5] 42.0-43.0 sec  1.07 GBytes  9.20 Gbits/sec
  [  8] 42.0-43.0 sec   949 MBytes  7.96 Gbits/sec
  [  9] 42.0-43.0 sec  1.06 GBytes  9.07 Gbits/sec
  [  4] 42.0-43.0 sec   484 MBytes  4.06 Gbits/sec
  [  3] 42.0-43.0 sec   329 MBytes  2.76 Gbits/sec
  [  6] 42.0-43.0 sec   641 MBytes  5.38 Gbits/sec
  [  7] 42.0-43.0 sec   599 MBytes  5.03 Gbits/sec
  [ 10] 42.0-43.0 sec   327 MBytes  2.74 Gbits/sec
  [SUM] 42.0-43.0 sec  5.38 GBytes  46.2 Gbits/sec
  [  5] 43.0-44.0 sec  1.11 GBytes  9.51 Gbits/sec
  [  8] 43.0-44.0 sec  1.16 GBytes  10.0 Gbits/sec
  [  9] 43.0-44.0 sec  1.09 GBytes  9.37 Gbits/sec
  [  4] 43.0-44.0 sec   553 MBytes  4.64 Gbits/sec
  [  3] 43.0-44.0 sec   340 MBytes  2.85 Gbits/sec
  [  6] 43.0-44.0 sec   763 MBytes  6.40 Gbits/sec
  [ 10] 43.0-44.0 sec   343 MBytes  2.88 Gbits/sec
  [  7] 43.0-44.0 sec   612 MBytes  5.14 Gbits/sec
  [SUM] 43.0-44.0 sec  5.91 GBytes  50.8 Gbits/sec
  [  5] 44.0-45.0 sec  1.09 GBytes  9.38 Gbits/sec
  [  6] 44.0-45.0 sec   765 MBytes  6.42 Gbits/sec
  [  8] 44.0-45.0 sec  1.08 GBytes  9.30 Gbits/sec
  [  9] 44.0-45.0 sec  1.18 GBytes  10.1 Gbits/sec
  [  3] 44.0-45.0 sec   340 MBytes  2.86 Gbits/sec
  [ 10] 44.0-45.0 sec   334 MBytes  2.80 Gbits/sec
  [  4] 44.0-45.0 sec   765 MBytes  6.42 Gbits/sec
  [  7] 44.0-45.0 sec   630 MBytes  5.29 Gbits/sec
  [SUM] 44.0-45.0 sec  6.12 GBytes  52.6 Gbits/sec
  [  8] 45.0-46.0 sec  1.05 GBytes  9.02 Gbits/sec
  [  9] 45.0-46.0 sec  1.13 GBytes  9.73 Gbits/sec
  [  4] 45.0-46.0 sec   366 MBytes  3.07 Gbits/sec
  [  3] 45.0-46.0 sec   214 MBytes  1.79 Gbits/sec
  [  6] 45.0-46.0 sec   504 MBytes  4.23 Gbits/sec
  [ 10] 45.0-46.0 sec   206 MBytes  1.72 Gbits/sec
  [  7] 45.0-46.0 sec   617 MBytes  5.17 Gbits/sec
  [  5] 45.0-46.0 sec  1.95 GBytes  16.7 Gbits/sec
  [SUM] 45.0-46.0 sec  5.99 GBytes  51.5 Gbits/sec
  [  5] 46.0-47.0 sec   475 MBytes  3.98 Gbits/sec
  [  9] 46.0-47.0 sec   573 MBytes  4.81 Gbits/sec
  [  3] 46.0-47.0 sec   203 MBytes  1.70 Gbits/sec
  [  7] 46.0-47.0 sec   633 MBytes  5.31 Gbits/sec
  [  8] 46.0-47.0 sec   818 MBytes  6.87 Gbits/sec
  [ 10] 46.0-47.0 sec   209 MBytes  1.75 Gbits/sec
  [  6] 46.0-47.0 sec   505 MBytes  4.24 Gbits/sec
  [  4] 46.0-47.0 sec   495 MBytes  4.16 Gbits/sec
  [SUM] 46.0-47.0 sec  3.82 GBytes  32.8 Gbits/sec
  [  5] 47.0-48.0 sec   986 MBytes  8.27 Gbits/sec
  [  8] 47.0-48.0 sec   642 MBytes  5.39 Gbits/sec
  [  6] 47.0-48.0 sec   340 MBytes  2.85 Gbits/sec
  [  9] 47.0-48.0 sec   693 MBytes  5.81 Gbits/sec
  [  7] 47.0-48.0 sec   651 MBytes  5.46 Gbits/sec
  [  4] 47.0-48.0 sec   329 MBytes  2.76 Gbits/sec
  [  3] 47.0-48.0 sec   207 MBytes  1.74 Gbits/sec
  [ 10] 47.0-48.0 sec   210 MBytes  1.76 Gbits/sec
  [SUM] 47.0-48.0 sec  3.96 GBytes  34.0 Gbits/sec
  [  4] 48.0-49.0 sec   405 MBytes  3.40 Gbits/sec
  [  7] 48.0-49.0 sec   694 MBytes  5.82 Gbits/sec
  [  6] 48.0-49.0 sec   505 MBytes  4.24 Gbits/sec
  [  3] 48.0-49.0 sec   208 MBytes  1.74 Gbits/sec
  [  8] 48.0-49.0 sec   746 MBytes  6.26 Gbits/sec
  [  9] 48.0-49.0 sec   780 MBytes  6.55 Gbits/sec
  [ 10] 48.0-49.0 sec   208 MBytes  1.75 Gbits/sec
  [  5] 48.0-49.0 sec   548 MBytes  4.59 Gbits/sec
  [SUM] 48.0-49.0 sec  4.00 GBytes  34.3 Gbits/sec
  [  7] 49.0-50.0 sec   846 MBytes  7.10 Gbits/sec
  [  3] 49.0-50.0 sec   212 MBytes  1.77 Gbits/sec
  [  9] 49.0-50.0 sec   602 MBytes  5.05 Gbits/sec
  [  5] 49.0-50.0 sec   596 MBytes  5.00 Gbits/sec
  [ 10] 49.0-50.0 sec   211 MBytes  1.77 Gbits/sec
  [  4] 49.0-50.0 sec   432 MBytes  3.62 Gbits/sec
  [  6] 49.0-50.0 sec   526 MBytes  4.41 Gbits/sec
  [  8] 49.0-50.0 sec   727 MBytes  6.10 Gbits/sec
  [SUM] 49.0-50.0 sec  4.05 GBytes  34.8 Gbits/sec
  [  5] 50.0-51.0 sec   686 MBytes  5.76 Gbits/sec
  [  8] 50.0-51.0 sec   740 MBytes  6.21 Gbits/sec
  [  7] 50.0-51.0 sec   933 MBytes  7.83 Gbits/sec
  [  6] 50.0-51.0 sec   480 MBytes  4.03 Gbits/sec
  [  9] 50.0-51.0 sec   853 MBytes  7.16 Gbits/sec
  [  3] 50.0-51.0 sec   318 MBytes  2.67 Gbits/sec
  [  4] 50.0-51.0 sec   517 MBytes  4.33 Gbits/sec
  [ 10] 50.0-51.0 sec   322 MBytes  2.70 Gbits/sec
  [SUM] 50.0-51.0 sec  4.74 GBytes  40.7 Gbits/sec
  [  5] 51.0-52.0 sec   881 MBytes  7.39 Gbits/sec
  [ 10] 51.0-52.0 sec   221 MBytes  1.85 Gbits/sec
  [  7] 51.0-52.0 sec   967 MBytes  8.11 Gbits/sec
  [  4] 51.0-52.0 sec   365 MBytes  3.06 Gbits/sec
  [  3] 51.0-52.0 sec   305 MBytes  2.56 Gbits/sec
  [  8] 51.0-52.0 sec   723 MBytes  6.07 Gbits/sec
  [  9] 51.0-52.0 sec   743 MBytes  6.23 Gbits/sec
  [  6] 51.0-52.0 sec   587 MBytes  4.93 Gbits/sec
  [SUM] 51.0-52.0 sec  4.68 GBytes  40.2 Gbits/sec
  [  5] 52.0-53.0 sec   790 MBytes  6.62 Gbits/sec
  [  6] 52.0-53.0 sec   666 MBytes  5.59 Gbits/sec
  [  8] 52.0-53.0 sec   919 MBytes  7.71 Gbits/sec
  [  7] 52.0-53.0 sec   889 MBytes  7.46 Gbits/sec
  [  3] 52.0-53.0 sec   309 MBytes  2.59 Gbits/sec
  [ 10] 52.0-53.0 sec   349 MBytes  2.93 Gbits/sec
  [  4] 52.0-53.0 sec   562 MBytes  4.72 Gbits/sec
  [  9] 52.0-53.0 sec   914 MBytes  7.67 Gbits/sec
  [SUM] 52.0-53.0 sec  5.27 GBytes  45.3 Gbits/sec
  [  5] 53.0-54.0 sec   709 MBytes  5.94 Gbits/sec
  [  6] 53.0-54.0 sec   533 MBytes  4.47 Gbits/sec
  [  8] 53.0-54.0 sec   905 MBytes  7.59 Gbits/sec
  [  7] 53.0-54.0 sec   765 MBytes  6.42 Gbits/sec
  [  9] 53.0-54.0 sec   833 MBytes  6.99 Gbits/sec
  [  4] 53.0-54.0 sec   607 MBytes  5.09 Gbits/sec
  [ 10] 53.0-54.0 sec   479 MBytes  4.02 Gbits/sec
  [  3] 53.0-54.0 sec   477 MBytes  4.00 Gbits/sec
  [SUM] 53.0-54.0 sec  5.18 GBytes  44.5 Gbits/sec
  [  8] 54.0-55.0 sec   864 MBytes  7.25 Gbits/sec
  [ 10] 54.0-55.0 sec   479 MBytes  4.02 Gbits/sec
  [  7] 54.0-55.0 sec  1.06 GBytes  9.07 Gbits/sec
  [  9] 54.0-55.0 sec   965 MBytes  8.10 Gbits/sec
  [  3] 54.0-55.0 sec   394 MBytes  3.30 Gbits/sec
  [  6] 54.0-55.0 sec   794 MBytes  6.66 Gbits/sec
  [  4] 54.0-55.0 sec   720 MBytes  6.04 Gbits/sec
  [  5] 54.0-55.0 sec  1.32 GBytes  11.3 Gbits/sec
  [SUM] 54.0-55.0 sec  6.49 GBytes  55.8 Gbits/sec
  [  5] 55.0-56.0 sec   580 MBytes  4.87 Gbits/sec
  [  6] 55.0-56.0 sec   738 MBytes  6.19 Gbits/sec
  [  8] 55.0-56.0 sec  1.02 GBytes  8.75 Gbits/sec
  [  7] 55.0-56.0 sec   948 MBytes  7.95 Gbits/sec
  [  9] 55.0-56.0 sec  1.02 GBytes  8.73 Gbits/sec
  [  3] 55.0-56.0 sec   550 MBytes  4.62 Gbits/sec
  [  4] 55.0-56.0 sec   733 MBytes  6.15 Gbits/sec
  [ 10] 55.0-56.0 sec   535 MBytes  4.49 Gbits/sec
  [SUM] 55.0-56.0 sec  6.02 GBytes  51.8 Gbits/sec
  [  5] 56.0-57.0 sec  1.25 GBytes  10.7 Gbits/sec
  [  6] 56.0-57.0 sec   870 MBytes  7.30 Gbits/sec
  [  8] 56.0-57.0 sec   997 MBytes  8.36 Gbits/sec
  [  7] 56.0-57.0 sec  1005 MBytes  8.43 Gbits/sec
  [  9] 56.0-57.0 sec  1.12 GBytes  9.63 Gbits/sec
  [  4] 56.0-57.0 sec   798 MBytes  6.69 Gbits/sec
  [  3] 56.0-57.0 sec   576 MBytes  4.83 Gbits/sec
  [ 10] 56.0-57.0 sec   573 MBytes  4.81 Gbits/sec
  [SUM] 56.0-57.0 sec  7.08 GBytes  60.8 Gbits/sec
  [  7] 57.0-58.0 sec  1.26 GBytes  10.8 Gbits/sec
  [  5] 57.0-58.0 sec   747 MBytes  6.27 Gbits/sec
  [  4] 57.0-58.0 sec   589 MBytes  4.94 Gbits/sec
  [  6] 57.0-58.0 sec   711 MBytes  5.96 Gbits/sec
  [  8] 57.0-58.0 sec   762 MBytes  6.39 Gbits/sec
  [ 10] 57.0-58.0 sec   476 MBytes  3.99 Gbits/sec
  [  3] 57.0-58.0 sec   532 MBytes  4.46 Gbits/sec
  [  9] 57.0-58.0 sec   671 MBytes  5.63 Gbits/sec
  [SUM] 57.0-58.0 sec  5.64 GBytes  48.4 Gbits/sec
  [  7] 58.0-59.0 sec   784 MBytes  6.57 Gbits/sec
  [  8] 58.0-59.0 sec   547 MBytes  4.59 Gbits/sec
  [  3] 58.0-59.0 sec   327 MBytes  2.74 Gbits/sec
  [  6] 58.0-59.0 sec   527 MBytes  4.42 Gbits/sec
  [  4] 58.0-59.0 sec   513 MBytes  4.30 Gbits/sec
  [ 10] 58.0-59.0 sec   361 MBytes  3.03 Gbits/sec
  [  5] 58.0-59.0 sec   771 MBytes  6.46 Gbits/sec
  [  9] 58.0-59.0 sec   422 MBytes  3.54 Gbits/sec
  [SUM] 58.0-59.0 sec  4.15 GBytes  35.7 Gbits/sec
  [  6] 59.0-60.0 sec   426 MBytes  3.57 Gbits/sec
  [  6]  0.0-60.0 sec  35.4 GBytes  5.07 Gbits/sec
  [  6] MSS size 1448 bytes (MTU 1500 bytes, ethernet)
  [  8] 59.0-60.0 sec   583 MBytes  4.89 Gbits/sec
  [  8]  0.0-60.0 sec  44.0 GBytes  6.30 Gbits/sec
  [  8] MSS size 1448 bytes (MTU 1500 bytes, ethernet)
  [  7] 59.0-60.0 sec   860 MBytes  7.21 Gbits/sec
  [  7]  0.0-60.0 sec  48.3 GBytes  6.92 Gbits/sec
  [  7] MSS size 1448 bytes (MTU 1500 bytes, ethernet)
  [  4] 59.0-60.0 sec   496 MBytes  4.16 Gbits/sec
  [  4]  0.0-60.0 sec  34.0 GBytes  4.86 Gbits/sec
  [  4] MSS size 1448 bytes (MTU 1500 bytes, ethernet)
  [  5] 59.0-60.0 sec   408 MBytes  3.42 Gbits/sec
  [  5]  0.0-60.0 sec  60.3 GBytes  8.63 Gbits/sec
  [  5] MSS size 1448 bytes (MTU 1500 bytes, ethernet)
  [  3] 59.0-60.0 sec   330 MBytes  2.77 Gbits/sec
  [  3]  0.0-60.1 sec  23.1 GBytes  3.31 Gbits/sec
  [  3] MSS size 1448 bytes (MTU 1500 bytes, ethernet)
  [ 10] 59.0-60.0 sec   360 MBytes  3.02 Gbits/sec
  [ 10]  0.0-60.2 sec  23.1 GBytes  3.29 Gbits/sec
  [ 10] MSS size 1448 bytes (MTU 1500 bytes, ethernet)
  [  9] 59.0-60.0 sec   419 MBytes  3.52 Gbits/sec
  [SUM] 59.0-60.0 sec  3.79 GBytes  32.6 Gbits/sec
  [  9]  0.0-60.3 sec  39.6 GBytes  5.65 Gbits/sec
  [  9] MSS size 1448 bytes (MTU 1500 bytes, ethernet)
  [SUM]  0.0-60.3 sec   308 GBytes  43.9 Gbits/sec


Error from psl01-gva-100g.cern.ch:
  No error.

Diagnostics from perfsonar-bandwidth.grid.surfsara.nl:
  No diagnostics.

Error from perfsonar-bandwidth.grid.surfsara.nl:
  iperf2 returned an error: Process took too long to run.

No further runs scheduled.
